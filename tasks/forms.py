from django.forms import ModelForm
from tasks.models import Task


# Feature 15 - create form for Task
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
