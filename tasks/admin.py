from django.contrib import admin
from tasks.models import Task


# Feature 12 - register Task model with admin
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
    )
