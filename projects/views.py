from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Feature 5 - create list view for projects
# Feature 8 - protect list view so only logged in user can access
@login_required
def list_projects(request):
    # Feature 8- change queryset to filter
    # the Project objects where only logged in user can view
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


# Feature 13 - allows people to see details about a project
@login_required
def show_project(request, pk):
    project = get_object_or_404(Project, pk=pk)
    context = {
        "project_object": project,
    }
    return render(request, "projects/detail.html", context)


# Feature 14 - create new project view function
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
